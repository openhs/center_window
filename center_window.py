#!/usr/bin/env python

# center_window.py
#
# Centers and restores position of an active window
#
# Copyright (C) 2020 - 2021 Róbert Čerňanský
# License: GNU GPLv3
# Version: 0.0.2



# Dependencies: xdotool, xwininfo



import argparse
import os
import re
import subprocess
from collections import namedtuple
from itertools import chain
from pathlib import Path
from tkinter import Tk



class ActiveWindow:

    def __init__(self):

        # gets output with following format:
        # WINDOW=<window_value>
        # X=<x_value>
        # Y=<y_value>
        # WIDTH=<width_value>
        # HEIGHT=<height_value>
        # ...
        self.__geometryOutputLines = subprocess.run(
            ["xdotool", "getactivewindow", "getwindowgeometry", "--shell"], capture_output = True, text = True)\
            .stdout.splitlines()
        self.__id = self.__parseValues(self.__geometryOutputLines, "WINDOW")[0]

        self.__positionsStorage = self._PositionsFileStorage(
            Path(os.path.expanduser(os.getenv("XDG_CONFIG_HOME", "~/.local/share/center_window"))))



    def centerOrRestore(self):
        storedPosition = self.__positionsStorage.retrieve(self.__id)
        if storedPosition is None:
            self.__center()
        else:
            self.__setPosition(storedPosition)



    def __center(self):

        def computeCenterPosition():
            mainWidget = Tk()
            windowSize = self._Size(*self.__parseValues(self.__geometryOutputLines, "WIDTH", "HEIGHT"))

            return self._Position(
                str(round(mainWidget.winfo_screenwidth() / 2 - int(windowSize.width) / 2)),
                str(round(mainWidget.winfo_screenheight() / 2 - int(windowSize.height) / 2)))



        # WORKAROUND: position returned by xdotool does not move the window to the same position when later using
        # windowmove command therefore we have to lower position values by the top and left border
        def adjustPositionByWindowBorders(position):
            wmHints = subprocess.run(["xwininfo", "-id", self.__id, "-wm"], capture_output = True, text = True).stdout

            # expects following format in wmHints:
            # ...
            # Frame ... <left>, <right>, <top>, <bottom>
            # ...
            leftBorderSize, topBorderSize = re.search(
                r"(\d+),\s+\d+,\s+(\d+),\s+\d+$",
                next(filter(lambda l: l.strip().startswith("Frame"), wmHints.splitlines()))
            ).groups()

            return self._Position(int(position.x) - int(leftBorderSize), int(position.y) - int(topBorderSize))



        self.__positionsStorage.store(self.__id, adjustPositionByWindowBorders(
            self._Position(*self.__parseValues(self.__geometryOutputLines, "X", "Y"))))
        self.__setPosition(computeCenterPosition())



    def __setPosition(self, position):
        self.__executeCommand("windowmove", arguments = position)



    def __executeCommand(self, command, *, options = None, arguments = None):
        if options is None: options = []
        if arguments is None: arguments = []

        return subprocess.run(
            ["xdotool", command, *options, self.__id, *arguments], capture_output = True, text = True).stdout



    @staticmethod
    def __parseValues(keyValList, *variables):
        return [re.search(r"^\w+=(\d+)$", filteredLine).group(1) for filteredLine in (
            filter(
                lambda l: l.startswith(tuple(map(lambda v: "{}=".format(v), variables))),
                keyValList))]



    class _PositionsFileStorage:
        def __init__(self, path):
            self.__filePath = path.joinpath("positions")

            if not os.path.isdir(path):
                os.makedirs(path)
            if not os.path.isfile(self.__filePath):
                self.__filePath.touch()



        def store(self, window, position):
            with open(self.__filePath) as positionsFile:
                entries = [self._KeyValEntry.fromStr(l) for l in positionsFile]

            entriesWithReplacedPosition =\
                chain((e for e in entries if e.key != window), (self._KeyValEntry(window, position),))

            with open(self.__filePath, "w") as positionsFile:
                positionsFile.writelines(map(lambda e: str(e) + "\n", entriesWithReplacedPosition))



        def retrieve(self, window):
            with open(self.__filePath) as positionsFile:
                entries = [self._KeyValEntry.fromStr(l) for l in positionsFile]
                entry = next(filter(lambda e: e.key == window, entries), None)

            with open(self.__filePath, "w") as positionsFile:
                positionsFile.writelines(map(lambda e: str(e) + "\n", filter(lambda e: e.key != window, entries)))

            return entry.values if entry is not None else None



        class _KeyValEntry:
            __VALUES_DELIMITER = ";"



            def __init__(self, key, values):
                self.__key = key
                self.__values = values



            @classmethod
            def fromStr(cls, entryString):
                keyVal = re.search(r"^(\w+)\s*=\s*(.*)$", str(entryString)).groups()
                values = keyVal[1].split(cls.__VALUES_DELIMITER)
                return cls(keyVal[0], values)



            @property
            def key(self):
                return self.__key



            @property
            def values(self):
                return self.__values



            def __str__(self):
                return "{} = {}".format(self.__key, self.__VALUES_DELIMITER.join(map(str, self.__values)))



    _Position = namedtuple("Position", ["x", "y"])
    _Size = namedtuple("Size", ["width", "height"])



if __name__ == "__main__":
    argparse.ArgumentParser(description = "Centers or restores position of an active window").parse_args()
    ActiveWindow().centerOrRestore()
